#! /bin/bash

#Install the three Node.js RPMs
rpm -Uvh nodejs-*.rpm

#Upgrade the Polarity Server
rpm -Uvh polarity-server-2.7.14-*.rpm

#Upgrade Polarity Web
rpm -Uvh polarity-web-2.7.5-*.rpm

#Upgrade the Polarity Integration Docs
rpm -Uvh polarity-integration-docs-2.7.0-*.rpm

#Upgrade the Polarity Administration Docs
rpm -Uvh polarity-admin-docs-2.7.1-*.rpm

#Verify that the Polarity version is now v2.7.14 and that all services are “active”
bash /app/polarity-server/scripts/polarity-status.sh

echo ""
echo ""
echo "All done now!"
echo ""
